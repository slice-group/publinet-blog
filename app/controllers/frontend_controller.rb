class FrontendController < ApplicationController
  layout 'layouts/frontend/application'
  
  def index
    @posts = KepplerBlog::Post.searching(params[:query]).where(public: true).page(@current_page).per(5)
    @posts_recents = @posts_recents = KepplerBlog::Post.where(public: true).order("created_at DESC").first(3)
    @categories = []

    KepplerBlog::Post.all.each do |post|
      @categories.push(KepplerBlog::Category.find(post.category_id)).uniq!
    end

  end
end
